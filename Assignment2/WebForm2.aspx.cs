﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment2
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        

        protected void Page_Load(object sender, EventArgs e)
        {
            int employees = 10;
            int managers = 3;
            int staff = employees + managers;
            string employeestring = employees.ToString();
            string slogan = "Best hotel in the Town!";
            
           // int days = Int32.Parse(checkOut)-Int32.Parse(checkIn);
           

            detail.InnerHtml = slogan;
            
            if (staff < 15 && managers < 5)
            {
                subheadings.InnerHtml += "<br> we are Hiring! <br>";
            }
            detail.InnerHtml += "<br>";

            

        }
        protected void Booking(object sender, EventArgs e)
        {
            string size = SIzeDropDownList.SelectedItem.Value.ToString();
            int roomReq = int.Parse(RoomDropDownList.Text);
            string name = clientName.Text.ToString();
            //string value = F.SelectedItem.Value.ToString();

            string gender;
            bool isChecked = F.Checked;
            if(isChecked==true)
            {
                gender = F.Text.ToString();
            }
            else
            {
                gender = M.Text.ToString();
            }
            
            //string gender = document.getElementById("Gender");
            string address = clientaddress.Text.ToString();
            string postal = clientPostalcode.Text.ToString();
            string phone = clientphone.Text.ToString();
            string email = clientemail.Text.ToString();


            Client newclient = new Client();
            newclient.ClientName = name;
            newclient.ClientAddress = address;
            newclient.ClientPostal = postal;
            newclient.ClientPhone = phone;
            newclient.ClientEmail = email;
            newclient.ClientGender = gender;

            Room newroom = new Room(roomReq, size);

            

            Booking newbooking = new Booking(newclient, newroom);
            BookingDiv.InnerHtml = newbooking.PrintReceipt();

        }

        protected void CheckOut_TextChanged(object sender, EventArgs e)
        {
                   }
    }

}