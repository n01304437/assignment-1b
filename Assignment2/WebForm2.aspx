﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm2.aspx.cs" Inherits="Assignment2.WebForm2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Assignment_2</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
         <h1>Hotel Booking Reservation</h1>
            <h2 id="subheadings" runat="server"></h2>
            <div id="detail" runat="server">

            </div>
            <asp:Panel ID="Client_Panel" runat="server" GroupingText="Client Information" Width="470px">
                <asp:Label ID="name" runat="server" Text="Name: "></asp:Label>
                <asp:TextBox ID="clientName" runat="server" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="NameValidator" runat="server" ControlToValidate="clientName" ErrorMessage="Please enter a name" ForeColor="Red"></asp:RequiredFieldValidator>
                <br/>
                <asp:Label ID="gender" runat="server" Text="Gender: "></asp:Label>
                <asp:RadioButton ID="M" runat="server"  Text="Male" GroupName="Gender" />                
                <asp:RadioButton ID="F" runat="server" Text="Female" GroupName="Gender" />
                <br />
                <asp:Label ID="address" runat="server" Text="Address:  "></asp:Label>
                <asp:TextBox ID="clientaddress" runat="server" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="AddressValidator" runat="server" ControlToValidate="clientaddress" ErrorMessage="Enter Address" ForeColor="Red"></asp:RequiredFieldValidator>
                <br />
                <asp:Label ID="phone" runat="server" Text="Phone Number:  "></asp:Label>
                <asp:TextBox ID="clientphone" runat="server" ></asp:TextBox>
                <asp:RegularExpressionValidator ID="phoneValidator" runat="server" ControlToValidate="clientphone" ErrorMessage="Invalid phone number" ForeColor="Red" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}"></asp:RegularExpressionValidator>
                <br />
                <asp:Label ID="postalcode" runat="server" Text="Postal Code:  "></asp:Label>
                <asp:TextBox ID="clientPostalcode" runat="server" ></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="clientPostalcode" ErrorMessage="Invalid postal code" ForeColor="Red" ValidationExpression="\d{5}(-\d{4})?"></asp:RegularExpressionValidator>
                <br />
                <asp:Label ID="email" runat="server" Text="Email:  "></asp:Label>
                <asp:TextBox ID="clientemail" runat="server" ></asp:TextBox>
                <asp:RegularExpressionValidator ID="EmailValidator" runat="server" ControlToValidate="clientemail" ErrorMessage="Invalid id" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ForeColor="Red"></asp:RegularExpressionValidator>
                <br />
                <asp:ValidationSummary ID="clientSummaryValidator" runat="server" ForeColor="Red" />
            </asp:Panel>

            <asp:Panel ID="Panel1" runat="server" GroupingText="Room Information" Width="470px">
                <asp:Label ID="room" runat="server" Text="Room Required: "></asp:Label>
                <asp:DropDownList ID="RoomDropDownList" runat="server">
                                <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                <asp:ListItem Value="6" Text="6"></asp:ListItem>
                                <asp:ListItem Value="7" Text="7"></asp:ListItem>
                            </asp:DropDownList>
                <br />
                <asp:Label ID="roomsize" runat="server" Text="Size of room:  "></asp:Label>
                <asp:DropDownList ID="SIzeDropDownList" runat="server">
                <asp:ListItem Value="S" Text="Small"></asp:ListItem>
                <asp:ListItem Value="M" Text="Medium"></asp:ListItem>
                <asp:ListItem Value="L" Text="Large"></asp:ListItem>
                                
                 </asp:DropDownList>
                      
                <br />

                <br />
            </asp:Panel>
            <asp:Button ID="Submit" runat="server" OnClick="Booking" Text="Submit" />

            <div runat="server" id="ClientDiv">

            </div>
            <div runat="server" id="RoomDiv">

            </div>
            <div runat="server" id="BookingDiv">

            </div>

            <footer runat="server" id="clientfooter">


            </footer>


        </div>
    </form>
</body>
</html>
