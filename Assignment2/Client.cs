﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment2
{
    public class Client
    {
        private string clientName;
        private string clientGender;
        private string clientAddress;
        private string clientPhone;
        private string clientPostal;
        private string clientEmail;


        public Client()
        {

        }
        public string ClientName
        {
            get { return clientName; }
            set { clientName = value; }
        }
        public string ClientGender
        {
            get { return clientGender; }
            set { clientGender = value; }
        }
        public string ClientAddress
        {
            get {return clientAddress;}
            set {clientAddress = value;}
        }

        public string ClientPhone
        {
            get {return clientPhone ;}
            set { clientPhone = value; }
        }
     public string ClientPostal
        {
            get {return clientPostal;}
            set {clientPostal = value;}
        }
     public string ClientEmail
        {
            get {return clientEmail;}
            set {clientEmail = value;}
        }
    }
   
}