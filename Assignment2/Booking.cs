﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment2
{
    public class Booking
    {
        public Client client;
        public Room room;

        public Booking(Client c, Room r)
        {
            client = c;
            room = r;
        }
        public string PrintReceipt()  // pending
        {
            string receipt = "Order Receipt: <br>";
            receipt += "Your total is" + CalculateBookingOrder().ToString() + "<br/>";
            receipt += "Name: " + client.ClientName+ "<br/>";
            receipt += "Gender:" + client.ClientGender + "<br/>";
            receipt += "Address: " + client.ClientAddress + "<br/>";
            receipt += "Postal Code: " + client.ClientPostal + "<br/>";
            receipt += "Phone: " + client.ClientPhone + "<br/>";
            receipt += "Email: " + client.ClientEmail + "<br/>";

            return receipt;
            
        }

        public double CalculateBookingOrder()
        {
            double total = 0;
            if(room.size == "S")
            {
                total = 500*room.roomReq;
               
            }
            else if(room.size =="M")
            {
                total = 800*room.roomReq;
            }
            else if (room.size =="L")
            {
                total = 1000*room.roomReq;
            }

           
            return total;
            
        }
    }
}